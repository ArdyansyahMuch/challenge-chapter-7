class ControllerView {
    static async registerView (req,res,next) {
        res.render('register')
    }

    static async loginView (req, res, next) {
        res.render('login')
    }

    static async AllUsers (req, res, next) {
        res.render('Users')
    }
}

module.exports = ControllerView