const jwt = require('jsonwebtoken')
const secret = "WawawawaW"

class Token {
    static generateToken (payload) {
        try {
            const token = jwt.sign (payload, secret)
            return token
        } catch (error) {
            console.log(error)
            return null
        }
    }

    static decodeToken (token) {
        try {
            const payload = jwt.verify(token, secret)
            return payload
        } catch (error) {
            console.log(error)
            return null
        }
    }
}
module.exports = Token