const route = require ('express').Router()
const { RoomController, UserController } = require ('../controller')
const { authentic , authorize } = require('../middleware')
const Middleware = require ('../middleware')
const ControllerView = require('../view.controllers')


route.post('/register', UserController.register)
route.post('/login', UserController.login )
route.get('/' , Middleware.authorize ,UserController.ShowAllUsers)

route.get('/view/register', ControllerView.registerView)
route.get('/view/login', ControllerView.loginView)
route.get('/view/users', ControllerView.AllUsers)

route.post('/create-room', RoomController.createRoom)
route.post('/fight/:id', Middleware.authentic ,RoomController.registerRoom)

module.exports = route