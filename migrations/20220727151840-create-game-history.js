'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('GameHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      RoomId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Rooms",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      winner: {
        type: Sequelize.INTEGER,
        references: {
          model: "Users",
          key: "id"
        },
      onUpdate: "cascade",
      onDelete: "cascade"
      },
      player1Picked: {
        type: Sequelize.STRING
      },
      player2Picked: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('GameHistories');
  }
};