const Token = require ('../utils')


class Middleware {
    static authentic(req, res, next){
        try {
            const token  = req.headers.token || req.body.token
            if(token) {
                const payload = Token.decodeToken(token)
                req.headers.userLogin = payload
                if(payload) {
                    
                    next()
                }else {
                    res.status(401).json( payload, "===========cek payload"
                        // {
                        //     msg : "Kamu tidak memiliki hak masuk 1"
                        // }
                    )
                }
            }else {
                    res.status(401).json( payload, "================cek payload juga"
                        // {
                        //     msg : "Kamu tidak memiliki hak masuk 2"
                        // }
                    )
                }
            }
         catch (error) {
            console.log(error)
            res.status(500).json(
                {
                    msg : "Terjadi kesalahan"
                }
            )
        }
    }

    static authorize (req, res, next) {
        try {
        const token  = req.headers.token || req.body.token
        const payload = Token.decodeToken(token)
        const _role = payload && payload.role ? payload.role : null
            if(_role === "SA") {
                req.headers.userLogin = payload
                next ()
            } 
            else {
                res.status(401).json(
                    {
                        msg: "Kamu tidak memiliki hak otorisasi"
                    }
                )
            }
            
        } catch (error) {
            console.log(error)
            res.status(500).json(
                {
                    msg : "Terjadi sebuah kesalahan"
                }
            )
        }

    }
}
module.exports = Middleware